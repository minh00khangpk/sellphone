package com.sellphone.dao;

import java.util.List;

import com.sellphone.entity.Category;

public interface AccountDAO {
	Category findById(Integer id);
	List<Category> findAll();
	Category create(Category entity);
	void update(Category entity);
	Category delete(Integer id);
}
