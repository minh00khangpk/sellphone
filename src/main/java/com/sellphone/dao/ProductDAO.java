package com.sellphone.dao;

import java.util.List;

import com.sellphone.entity.Products;

public interface ProductDAO {
	Products findById(Integer id);
	List<Products> findAll();
	Products create(Products entity);
	void update(Products entity);
	Products delete(Integer id);
}
