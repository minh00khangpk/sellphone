package com.sellphone.dao;

import java.util.List;

import javax.persistence.TypedQuery;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.sellphone.entity.Products;

@Transactional
public class ProductDAOImpl implements ProductDAO {
	
	@Autowired
	SessionFactory factory;

	@Override
	public Products findById(Integer id) {
		Session session = factory.getCurrentSession();
		Products entity = session.find(Products.class, id);
		return entity;
	}

	@Override
	public List<Products> findAll() {
		String hql = "FROM Products";
		Session session = factory.getCurrentSession();
		TypedQuery<Products> query = session.createQuery(hql, Products.class);
		List<Products> list = query.getResultList();
		return list;
	}

	@Override
	public Products create(Products entity) {
		Session session = factory.getCurrentSession();
		session.save(entity);
		return entity;
	}

	@Override
	public void update(Products entity) {
		Session session = factory.getCurrentSession();
		session.update(entity);
	}

	@Override
	public Products delete(Integer id) {
		Session session = factory.getCurrentSession();
		Products entity = session.find(Products.class, id);
		session.delete(entity);
		return entity;
	}

}
