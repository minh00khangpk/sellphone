package com.sellphone.entity;
// Generated May 24, 2020 2:32:02 PM by Hibernate Tools 5.1.10.Final

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * Category generated by hbm2java
 */
@Entity
@Table(name = "category", catalog = "sellphone", uniqueConstraints = @UniqueConstraint(columnNames = "name"))
public class Category implements java.io.Serializable {

	private Integer id;
	private String name;
	private String namevn;
	private Set<Products> productses = new HashSet<Products>(0);
	private Set<Products> productses_1 = new HashSet<Products>(0);

	public Category() {
	}

	public Category(String name, String namevn) {
		this.name = name;
		this.namevn = namevn;
	}

	public Category(String name, String namevn, Set<Products> productses, Set<Products> productses_1) {
		this.name = name;
		this.namevn = namevn;
		this.productses = productses;
		this.productses_1 = productses_1;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "name", unique = true, nullable = false)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "namevn", nullable = false)
	public String getNamevn() {
		return this.namevn;
	}

	public void setNamevn(String namevn) {
		this.namevn = namevn;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "category")
	public Set<Products> getProductses() {
		return this.productses;
	}

	public void setProductses(Set<Products> productses) {
		this.productses = productses;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "category")
	public Set<Products> getProductses_1() {
		return this.productses_1;
	}

	public void setProductses_1(Set<Products> productses_1) {
		this.productses_1 = productses_1;
	}

}
