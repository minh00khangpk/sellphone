package com.sellphone.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sellphone.dao.CategoryDAO;
import com.sellphone.entity.Category;

@Controller
public class HomeController {
	@Autowired
	CategoryDAO cate;
	
	@RequestMapping("/home/index")
	public String index() {
		System.out.println(cate.findById(1).getName());
		return "home/index";
	}
	@RequestMapping("/home/about")
	public String about() {
		return "home/about";
	}
	@RequestMapping("/home/contact")
	public String contact() {
		return "home/contact";
	}
	@RequestMapping("/home/feedback")
	public String feedback() {
		return "home/feedback";
	}
	@RequestMapping("/home/faq")
	public String faq() {
		return "home/faq";
	}
}
