<%@ page pageEncoding="UTF-8"%>
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="/home/index">Home</a>
    </div>
    <ul class="nav navbar-nav">
      <li><a href="/home/about">About us</a></li>
      <li><a href="/home/contact">Contact us</a></li>
      <li><a href="/home/feedback">Feedback</a></li>
      <li><a href="/home/faq">FAQs</a></li>
      <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Account
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="#">Đăng nhập</a></li>
          <li><a href="#">Đăng ký</a></li>
          <li><a href="#">Quên mật khẩu</a></li>
          <li><a href="#">Đăng xuất</a></li>
          <li><a href="#">Đăng ký</a></li>
          <li><a href="#">Đơn hàng</a></li>
        </ul>
      </li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <li>
      	<a href="#"> Tiếng việt</a>
      </li>
      <li>
      	<a href="#"> English</a>
      </li>
    </ul>
  </div>
</nav>