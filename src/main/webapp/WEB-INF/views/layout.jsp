<%@ page pageEncoding="utf-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Sell Phone</title>
	<tiles:insertAttribute name="head"/>
</head>
<body>

	<div class="container">
		<header class="row">
			<h2 class="alert alert-success">Hello</h2>
		</header>
		<nav class="row">
			<tiles:insertAttribute name="menu"/>
		</nav>
		<div class="row">
			<article class="col-sm-9">
				<tiles:insertAttribute name="body"/>
			</article>
			<aside class="col-sm-3"> 
				<tiles:insertAttribute name="aside"/>
			</aside>
		</div>
			<footer class="row">
				<p class="text-center">&copy; 2020. All rights reserved</p>
			</footer>
	</div>

</body>
</html>